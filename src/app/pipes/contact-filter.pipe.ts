import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'contactPipe' })
export class ContactPipe implements PipeTransform {
  transform(contacts: any[], type: string): any[] {
    return contacts.filter(item => item.ContactType == type)
  }
}