import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'searchPipe' })
export class SearchPipe implements PipeTransform {
  transform(branches: any[], townName: string): any[] {
    if (townName === '') {
      return branches;
    }
    else {

      return branches.filter(item => item.PostalAddress.TownName == townName)
    }
  }
}