import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BranchesListComponent } from './pages/branches-list/branches-list.component';
import { BranchDetailsComponent } from './pages/branch-details/branch-details.component';
import { HttpClientModule } from '@angular/common/http';
import { ContactPipe } from './pipes/contact-filter.pipe';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BranchesListComponent,
    BranchDetailsComponent,
    ContactPipe,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
