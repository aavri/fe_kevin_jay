import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class BranchesService {
  constructor(private httpClient: HttpClient) {

  }

  getBranchesList(): Observable<any> {
    return this.httpClient.get('https://api.halifax.co.uk/open-banking/v2.2/branches').pipe(map(res => res))
  }

  getSeletedBranch(Identification) {
    return this.httpClient.get('https://api.halifax.co.uk/open-banking/v2.2/branches').pipe(map((res: any) => {
      return res.data[0].Brand[0].Branch.filter(item => item.Identification == Identification)
    }))
  }
}