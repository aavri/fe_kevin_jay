import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchesListComponent } from './pages/branches-list/branches-list.component';
import { BranchDetailsComponent } from './pages/branch-details/branch-details.component';

const routes: Routes = [
  {
    path: '',
    component: BranchesListComponent
  }, {
    path: 'branch-details/:id',
    component: BranchDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
