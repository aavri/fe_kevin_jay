import { Component, OnInit } from '@angular/core';
import { BranchesService } from 'src/app/services/branches.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-branches-list',
  templateUrl: './branches-list.component.html',
  styleUrls: ['./branches-list.component.css']
})
export class BranchesListComponent implements OnInit {
  branchesList: any;
  cityName: string;
  townName = ''
  constructor(private branchService: BranchesService, private router: Router) { }

  ngOnInit() {
    this.branchService.getBranchesList().subscribe(res => {
      this.branchesList = res.data[0].Brand[0].Branch
    });
    // this.loadBranches();
  }


  loadBranches() {
    this.townName = '';
  }


  searchByCity() {
    this.townName = this.cityName.toUpperCase()
  }

  navigateTo(id) {
    this.router.navigate(['branch-details', id])
  }
}

