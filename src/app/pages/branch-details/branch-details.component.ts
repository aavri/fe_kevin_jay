import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BranchesService } from 'src/app/services/branches.service';

@Component({
  selector: 'app-branch-details',
  templateUrl: './branch-details.component.html',
  styleUrls: ['./branch-details.component.css']
})
export class BranchDetailsComponent implements OnInit {
  id: any;
  branchDetail: any;
  constructor(private route: ActivatedRoute, private service: BranchesService) {
    this.route.params.subscribe(param => {
      this.id = param.id
    })
  }

  ngOnInit() {
    this.service.getSeletedBranch(this.id).subscribe(res => {
      this.branchDetail = res[0]
    })
  }

}
